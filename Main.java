package collections;

import collections.Cart;

import java.util.*;

public class Main {
    /*
    There is other ways of doing this.
    Since the assignment was after collections,
    So my focus was using on collections rather than design
     */
    public static void main(String[] args) {
        Website website = new Website();

        // Owners adding items
        website.addItems("Zara",new HashSet<>(Arrays.asList("Shoes","Shirts")));
        website.addItems("Puma",new HashSet<>(Arrays.asList("Shoes","Shirts")));
        website.addItems("Biba",new HashSet<>(Arrays.asList("Kurtas","Legins")));

        // Remove item by brand name
        website.removeItemByBrand("Zara");

        // Remove accessories from Brand
        website.removeAccessories("Biba",new HashSet<>(Arrays.asList("Kurtas")));

        // Get accessories by brand Name
        Set<String> puma = website.getItemsByBrand("Puma");

        // Get all items
        Map<String, Set<String>> allItems = website.getAllItems();
        System.out.println(allItems);

        // Add to collections.Cart
        Cart cart = new Cart();
        System.out.println("adding values to Cart");
        cart.addToCart("Zara","Purses");
        cart.addToCart("Zara","Shoes");
        cart.addToCart("Zara","Shoes");
        cart.addToCart("Puma","Shoes");
        cart.addToCart("BiBa","Western ware");

        cart.printCartItems();

        // Remove from Cart
        System.out.println("Removing value from cart");
        cart.removeFromCart("Zara","Purses");
        System.out.println("\nCart after removing items");
        cart.printCartItems();

    }
}
