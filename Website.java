package collections;

import java.util.*;

public class Website {

    private static final Map<String, Set<String>> data = new HashMap<>();

    public static void print(Map<String, Set<String>> data)
    {
        for (Map.Entry entry: data.entrySet())
        {
            System.out.println(entry.getKey() + " : "+ entry.getValue());
        }
    }

    public void addItems(String brandName, Set<String> accessories)
    {
        if(data.containsKey(brandName))
        {
            Set<String> acc = data.get(brandName);
            acc.addAll(accessories);
        }
        else {
            data.put(brandName, accessories);
        }
    }

    public void removeItemByBrand(String brandName)
    {
            data.remove(brandName);
    }

    public void removeAccessories(String brandName, Set<String> accessories)
    {
        if(data.containsKey(brandName))
        {
            Set<String> acc = data.get(brandName);
                for (String s:accessories)
                {
                   // if(acc.contains(s))
                        acc.remove(s);
                }
        }
    }

    public Set<String> getItemsByBrand(String brandName)
    {
       return Collections.unmodifiableSet(data.get(brandName));
    }

    public Map<String,Set<String>> getAllItems()
    {
        Map<String,Set<String>> dataCopy = new HashMap<>();
        for (Map.Entry<String,Set<String>> entry: data.entrySet())
        {
            dataCopy.put(entry.getKey(),Collections.unmodifiableSet(entry.getValue()));
        }
      return Collections.unmodifiableMap(dataCopy);
    }

}
