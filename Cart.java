package collections;

import java.util.HashMap;
import java.util.Map;

public class Cart {
    Map<String, Integer> cart = new HashMap<>();

    public void addToCart(String brandName,String accessories)
    {
        String key = brandName+":"+accessories;
        if(cart.containsKey(key))
        {
            cart.put(key,cart.get(key)+1);
        }
        else
            cart.put(key,1);
    }

    public void removeFromCart(String brandName,String accessories)
    {
        String key = brandName+":"+accessories;
        cart.remove(key);
    }

    public void printCartItems()
    {
        for(Map.Entry entry: cart.entrySet())
        {
            System.out.println(entry.getKey()+" : "+ entry.getValue());
        }
    }
}
